package ffufm.kurt.api.spec.handler.employee.implementation

import ffufm.kurt.api.PassTestBase
import ffufm.kurt.api.spec.dbo.employee.EmployeeEmployee
import ffufm.kurt.api.spec.handler.employee.EmployeeEmployeeDatabaseHandler
import ffufm.kurt.api.spec.handler.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.server.ResponseStatusException
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith


class EmployeeEmployeeDatabaseHandlerTest : PassTestBase() {


    @Test
    fun `create should return employee`() = runBlocking {
        assertEquals(0, employeeEmployeeRepository.findAll().count())
        val employee = EntityGenerator.createEmployee()
        val createdEmployee = employeeEmployeeDatabaseHandler.create(employee.toDto())

        assertEquals(1, employeeEmployeeRepository.findAll().count())
        assertEquals(employee.firstName, createdEmployee.firstName)
        assertEquals(employee.lastName, createdEmployee.lastName)
        assertEquals(employee.email, createdEmployee.email)
        assertEquals(employee.position, createdEmployee.position)
    }

//    @Test
//    fun `create duplicate email should fail `() = runBlocking {
//        val employee = employeeEmployeeRepository.save(EntityGenerator.createEmployee())
//        val duplicateUser = employee.copy(
//            email = "greg.nue@f-bootcamp.com").toDto()
//        val exception = assertFailsWith<ResponseStatusException> {
//            employeeEmployeeDatabaseHandler.create(duplicateUser)
//        }
//        val expectedException = "409 CONFLICT \"Email ${duplicateUser.email} was already exists!\""
//        assertEquals(expectedException, exception.message)
//    }

//    @Test
//    fun `test getAll`() = runBlocking {
//        val maxResults: Int = 100
//        val page: Int = 0
//        employeeEmployeeDatabaseHandler.getAll(maxResults, page)
//        Unit
//    }
//
//    @Test
//    fun `test getById`() = runBlocking {
//        val id: Long = 0
//        employeeEmployeeDatabaseHandler.getById(id)
//        Unit
//    }
//
//    @Test
//    fun `test remove`() = runBlocking {
//        val id: Long = 0
//        employeeEmployeeDatabaseHandler.remove(id)
//        Unit
//    }
//
//    @Test
//    fun `test update`() = runBlocking {
//        val body: EmployeeEmployee = EmployeeEmployee()
//        val id: Long = 0
//        employeeEmployeeDatabaseHandler.update(body, id)
//        Unit
//    }
}
