package ffufm.kurt.api.spec.handler.employee.integration

import com.fasterxml.jackson.databind.ObjectMapper
import ffufm.kurt.api.PassTestBase
import ffufm.kurt.api.repositories.employee.EmployeeContactRepository
import ffufm.kurt.api.spec.dbo.employee.EmployeeContact
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class EmployeeContactHandlerTest : PassTestBase() {
    @Autowired
    private lateinit var employeeContactRepository: EmployeeContactRepository

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var mockMvc: MockMvc
//
//    @Before
//    @After
//    fun cleanRepositories() {
//        employeeContactRepository.deleteAll()
//    }
//
//    @Test
//    @WithMockUser
//    fun `test createContacts`() {
//        val body: EmployeeContact = EmployeeContact()
//        val id: String = ""
//                mockMvc.post("/employees/{id}/contacts/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//                    content = objectMapper.writeValueAsString(body)
//                }.andExpect {
//                    status { isOk }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test deleteContacts`() {
//        val id: Long = 0
//                mockMvc.delete("/employees/contacts/{id}/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//
//                }.andExpect {
//                    status { isOk }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test updateContacts`() {
//        val body: EmployeeContact = EmployeeContact()
//        val id: Long = 0
//                mockMvc.put("/employees/contacts/{id}/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//                    content = objectMapper.writeValueAsString(body)
//                }.andExpect {
//                    status { isOk }
//
//                }
//    }
}
