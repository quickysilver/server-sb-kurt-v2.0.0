package ffufm.kurt.api.spec.handler.utils

import ffufm.kurt.api.spec.dbo.employee.EmployeeEmployee

object EntityGenerator {
    fun createEmployee(): EmployeeEmployee = EmployeeEmployee(
        firstName = "Greg",
        lastName = "Nue",
        email = "greg.nue@f-bootcamp.com",
        position = "Monteur"
    )
}