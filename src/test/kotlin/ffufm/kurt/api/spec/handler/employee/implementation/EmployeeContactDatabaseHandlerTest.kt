package ffufm.kurt.api.spec.handler.employee.implementation

import ffufm.kurt.api.PassTestBase
import ffufm.kurt.api.repositories.employee.EmployeeContactRepository
import ffufm.kurt.api.spec.dbo.employee.EmployeeContact
import ffufm.kurt.api.spec.handler.employee.EmployeeContactDatabaseHandler
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired

class EmployeeContactDatabaseHandlerTest : PassTestBase() {
    @Autowired
    lateinit var employeeContactRepository: EmployeeContactRepository

    @Autowired
    lateinit var employeeContactDatabaseHandler: EmployeeContactDatabaseHandler

//    @Before
//    @After
//    fun cleanRepositories() {
//        employeeContactRepository.deleteAll()
//    }
//
//    @Test
//    fun `test createContacts`() = runBlocking {
//        val body: EmployeeContact = EmployeeContact()
//        val id: String = ""
//        employeeContactDatabaseHandler.createContacts(body, id)
//        Unit
//    }
//
//    @Test
//    fun `test deleteContacts`() = runBlocking {
//        val id: Long = 0
//        employeeContactDatabaseHandler.deleteContacts(id)
//        Unit
//    }
//
//    @Test
//    fun `test updateContacts`() = runBlocking {
//        val body: EmployeeContact = EmployeeContact()
//        val id: Long = 0
//        employeeContactDatabaseHandler.updateContacts(body, id)
//        Unit
//    }
}
