package ffufm.kurt.api.spec.handler.superior.integration

import com.fasterxml.jackson.databind.ObjectMapper
import ffufm.kurt.api.PassTestBase
import ffufm.kurt.api.repositories.superior.SuperiorSuperiorRepository
import ffufm.kurt.api.spec.dbo.superior.SuperiorSuperior
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class SuperiorSuperiorHandlerTest : PassTestBase() {
    @Autowired
    private lateinit var superiorSuperiorRepository: SuperiorSuperiorRepository

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var mockMvc: MockMvc
//
//    @Before
//    @After
//    fun cleanRepositories() {
//        superiorSuperiorRepository.deleteAll()
//    }
//
//    @Test
//    @WithMockUser
//    fun `test create`() {
//        val body: SuperiorSuperior = SuperiorSuperior()
//                mockMvc.post("/superiors/") {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//                    content = objectMapper.writeValueAsString(body)
//                }.andExpect {
//                    status { isOk }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test getById`() {
//        val id: Long = 0
//                mockMvc.get("/superiors/{id}/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//
//                }.andExpect {
//                    status { isOk }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test remove`() {
//        val id: Long = 0
//                mockMvc.delete("/superiors/{id}/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//
//                }.andExpect {
//                    status { isOk }
//
//                }
//    }
//
//    @Test
//    @WithMockUser
//    fun `test update`() {
//        val body: SuperiorSuperior = SuperiorSuperior()
//        val id: Long = 0
//                mockMvc.put("/superiors/{id}/", id) {
//                    accept(MediaType.APPLICATION_JSON)
//                    contentType = MediaType.APPLICATION_JSON
//                    content = objectMapper.writeValueAsString(body)
//                }.andExpect {
//                    status { isOk }
//
//                }
//    }
}
