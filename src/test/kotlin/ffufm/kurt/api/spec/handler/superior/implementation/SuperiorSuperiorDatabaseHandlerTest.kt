package ffufm.kurt.api.spec.handler.superior.implementation

import ffufm.kurt.api.PassTestBase
import ffufm.kurt.api.repositories.superior.SuperiorSuperiorRepository
import ffufm.kurt.api.spec.dbo.superior.SuperiorSuperior
import ffufm.kurt.api.spec.handler.superior.SuperiorSuperiorDatabaseHandler
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired

class SuperiorSuperiorDatabaseHandlerTest : PassTestBase() {
    @Autowired
    lateinit var superiorSuperiorRepository: SuperiorSuperiorRepository

    @Autowired
    lateinit var superiorSuperiorDatabaseHandler: SuperiorSuperiorDatabaseHandler

//    @Before
//    @After
//    fun cleanRepositories() {
//        superiorSuperiorRepository.deleteAll()
//    }
//
//    @Test
//    fun `test create`() = runBlocking {
//        val body: SuperiorSuperior = SuperiorSuperior()
//        superiorSuperiorDatabaseHandler.create(body)
//        Unit
//    }
//
//    @Test
//    fun `test getById`() = runBlocking {
//        val id: Long = 0
//        superiorSuperiorDatabaseHandler.getById(id)
//        Unit
//    }
//
//    @Test
//    fun `test remove`() = runBlocking {
//        val id: Long = 0
//        superiorSuperiorDatabaseHandler.remove(id)
//        Unit
//    }
//
//    @Test
//    fun `test update`() = runBlocking {
//        val body: SuperiorSuperior = SuperiorSuperior()
//        val id: Long = 0
//        superiorSuperiorDatabaseHandler.update(body, id)
//        Unit
//    }
}
