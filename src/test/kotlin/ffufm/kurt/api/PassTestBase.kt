package ffufm.kurt.api

import de.ffuf.pass.common.security.SpringContext
import de.ffuf.pass.common.security.SpringSecurityAuditorAware
import ffufm.kurt.api.repositories.employee.EmployeeAddressRepository
import ffufm.kurt.api.repositories.employee.EmployeeEmployeeRepository
import ffufm.kurt.api.spec.handler.employee.EmployeeAddressDatabaseHandler
import ffufm.kurt.api.spec.handler.employee.EmployeeEmployeeDatabaseHandler
import org.junit.After
import org.junit.Before
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ApplicationContext
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@ActiveProfiles("test")
@SpringBootTest(classes = [SBKurt::class, SpringSecurityAuditorAware::class])
@AutoConfigureMockMvc
abstract class PassTestBase {
    @Autowired
    lateinit var context: ApplicationContext

    @Autowired
    lateinit var employeeEmployeeRepository: EmployeeEmployeeRepository

    @Autowired
    lateinit var employeeEmployeeDatabaseHandler: EmployeeEmployeeDatabaseHandler

    @Autowired
    lateinit var employeeAddressRepository: EmployeeAddressRepository

    @Autowired
    lateinit var employeeAddressDatabaseHandler: EmployeeAddressDatabaseHandler


    @Before
    fun initializeContext() {
        SpringContext.context = context
    }

    @After
    fun cleanRepositories() {
        employeeEmployeeRepository.deleteAll()
        employeeAddressRepository.deleteAll()
    }
}
