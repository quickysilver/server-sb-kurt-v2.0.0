package ffufm.kurt.api.repositories.superior

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.kurt.api.spec.dbo.superior.SuperiorSuperior
import kotlin.Long
import org.springframework.stereotype.Repository

@Repository
interface SuperiorSuperiorRepository : PassRepository<SuperiorSuperior, Long>
