package ffufm.kurt.api.repositories.task

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.kurt.api.spec.dbo.task.TaskTask
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface TaskTaskRepository : PassRepository<TaskTask, Long> {
    @Query(
        "SELECT t from TaskTask t LEFT JOIN FETCH t.employee",
        countQuery = "SELECT count(id) FROM TaskTask"
    )
    fun findAllAndFetchEmployee(pageable: Pageable): Page<TaskTask>

    @Query(
        "SELECT t from TaskTask t LEFT JOIN FETCH t.project",
        countQuery = "SELECT count(id) FROM TaskTask"
    )
    fun findAllAndFetchProject(pageable: Pageable): Page<TaskTask>
}
