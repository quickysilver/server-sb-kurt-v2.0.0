package ffufm.kurt.api.spec.dbo.superior

import am.ik.yavi.builder.ValidatorBuilder
import am.ik.yavi.builder.konstraint
import am.ik.yavi.builder.konstraintOnObject
import de.ffuf.pass.common.models.PassDTO
import de.ffuf.pass.common.models.PassDTOModel
import de.ffuf.pass.common.models.PassDtoSerializer
import de.ffuf.pass.common.models.PassModelValidation
import de.ffuf.pass.common.models.idDto
import de.ffuf.pass.common.security.SpringContext
import de.ffuf.pass.common.utilities.extensions.konstraint
import de.ffuf.pass.common.utilities.extensions.toEntities
import de.ffuf.pass.common.utilities.extensions.toSafeDtos
import java.util.TreeSet
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Index
import javax.persistence.Lob
import javax.persistence.SequenceGenerator
import javax.persistence.Table
import javax.persistence.UniqueConstraint
import kotlin.Long
import kotlin.String
import kotlin.reflect.KClass
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.FetchMode
import org.springframework.beans.factory.getBeansOfType
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

/**
 * Superiors in the company
 */
@Entity(name = "SuperiorSuperior")
@Table(name = "superior_superior")
data class SuperiorSuperior(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    /**
     * first name of the superior
     * Sample: Andero
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "first_name"
    )
    @Lob
    val firstName: String = "",
    /**
     * lastName of superior
     * Sample: Mustermann
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "last_name"
    )
    @Lob
    val lastName: String = "",
    /**
     * email of the superior
     * Sample: andero.mustermann@f-bootcamp.com
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "email"
    )
    @Lob
    val email: String = ""
) : PassDTOModel<SuperiorSuperior, SuperiorSuperiorDTO, Long>() {
    override fun toDto(): SuperiorSuperiorDTO =
            super.toDtoInternal(SuperiorSuperiorSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<SuperiorSuperior, SuperiorSuperiorDTO, Long>,
            SuperiorSuperiorDTO, Long>>)

    override fun readId(): Long? = this.id

    override fun toString(): String = super.toString()
}

/**
 * Superiors in the company
 */
data class SuperiorSuperiorDTO(
    val id: Long? = null,
    /**
     * first name of the superior
     * Sample: Andero
     */
    val firstName: String? = "",
    /**
     * lastName of superior
     * Sample: Mustermann
     */
    val lastName: String? = "",
    /**
     * email of the superior
     * Sample: andero.mustermann@f-bootcamp.com
     */
    val email: String? = ""
) : PassDTO<SuperiorSuperior, Long>() {
    override fun toEntity(): SuperiorSuperior =
            super.toEntityInternal(SuperiorSuperiorSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<SuperiorSuperior, PassDTO<SuperiorSuperior, Long>,
            Long>, PassDTO<SuperiorSuperior, Long>, Long>>)

    override fun readId(): Long? = this.id
}

@Component
class SuperiorSuperiorSerializer : PassDtoSerializer<SuperiorSuperior, SuperiorSuperiorDTO, Long>()
        {
    override fun toDto(entity: SuperiorSuperior): SuperiorSuperiorDTO = cycle(entity) {
        SuperiorSuperiorDTO(
                id = entity.id,
        firstName = entity.firstName,
        lastName = entity.lastName,
        email = entity.email,

                )}

    override fun toEntity(dto: SuperiorSuperiorDTO): SuperiorSuperior = SuperiorSuperior(
            id = dto.id,
    firstName = dto.firstName ?: "",
    lastName = dto.lastName ?: "",
    email = dto.email ?: "",

            )
    override fun idDto(id: Long): SuperiorSuperiorDTO = SuperiorSuperiorDTO(
            id = id,
    firstName = null,
    lastName = null,
    email = null,

            )}

@Service("superior.SuperiorSuperiorValidator")
class SuperiorSuperiorValidator : PassModelValidation<SuperiorSuperior> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<SuperiorSuperior>):
            ValidatorBuilder<SuperiorSuperior> = validatorBuilder.apply {
    }
}

@Service("superior.SuperiorSuperiorDTOValidator")
class SuperiorSuperiorDTOValidator : PassModelValidation<SuperiorSuperiorDTO> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<SuperiorSuperiorDTO>):
            ValidatorBuilder<SuperiorSuperiorDTO> = validatorBuilder.apply {
    }
}
