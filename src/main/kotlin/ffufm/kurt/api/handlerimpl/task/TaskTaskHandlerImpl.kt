package ffufm.kurt.api.handlerimpl.task

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffufm.kurt.api.repositories.task.TaskTaskRepository
import ffufm.kurt.api.spec.dbo.task.TaskTask
import ffufm.kurt.api.spec.dbo.task.TaskTaskDTO
import ffufm.kurt.api.spec.handler.task.TaskTaskDatabaseHandler
import kotlin.Long
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component

@Component("task.TaskTaskHandler")
class TaskTaskHandlerImpl : PassDatabaseHandler<TaskTask, TaskTaskRepository>(),
        TaskTaskDatabaseHandler {
    //    /**
//     * :
//     */
//    override suspend fun createTasks(): TaskTask {
//        TODO("not checked yet")
//        return repository.save(body)
//    }
//
//    /**
//     * Delete Task by id.: Deletes one specific Task.
//     * HTTP Code 200: Successfully deleted task
//     */
//    override suspend fun remove(id: Long): TaskTask {
//        val original = repository.findById(id).orElseThrow404(id)
//        TODO("not checked yet - update the values you really want updated")
//        return repository.delete(original)
//    }
//
//    /**
//     * Update the Task: Updates an existing Task
//     * HTTP Code 200: The updated model
//     * HTTP Code 404: The requested object could not be found by the submitted id.
//     * HTTP Code 422: On or many fields contains a invalid value.
//     */
//    override suspend fun update(body: TaskTask, id: Long): TaskTask {
//        val original = repository.findById(id).orElseThrow404(id)
//        TODO("not checked yet - update the values you really want updated")
//        return repository.save(original)
//    }
    override suspend fun createTasks(body: TaskTaskDTO, employeeId: Long, projectId: Long): ResponseEntity<Any> {
        TODO("Not yet implemented")
    }

    override suspend fun remove(id: Long): TaskTaskDTO {
        TODO("Not yet implemented")
    }

    override suspend fun update(body: TaskTaskDTO, id: Long): TaskTaskDTO {
        TODO("Not yet implemented")
    }
}
