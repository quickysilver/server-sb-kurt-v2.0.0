package ffufm.kurt.api.handlerimpl.project

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffufm.kurt.api.repositories.project.ProjectProjectRepository
import ffufm.kurt.api.spec.dbo.project.ProjectProject
import ffufm.kurt.api.spec.dbo.project.ProjectProjectDTO
import ffufm.kurt.api.spec.handler.project.ProjectProjectDatabaseHandler
import kotlin.Int
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component

@Component("project.ProjectProjectHandler")
class ProjectProjectHandlerImpl : PassDatabaseHandler<ProjectProject, ProjectProjectRepository>(),
        ProjectProjectDatabaseHandler {
    //    /**
//     * Create Project: Creates a new Project object
//     * HTTP Code 201: The created Project
//     */
//    override suspend fun create(body: ProjectProject): ProjectProject {
//        TODO("not checked yet")
//        return repository.save(body)
//    }
//
//    /**
//     * Get all Projects: Returns all Projects from the system that the user has access to.
//     * HTTP Code 200: List of Projects
//     */
//    override suspend fun getAll(maxResults: Int = 100, page: Int = 0): Page<ProjectProject> {
//        TODO("not checked yet")
//        return repository.findAll(Pageable.unpaged())
//    }
//
//    /**
//     * Delete Project by id.: Deletes one specific Project.
//     * HTTP Code 200: Successfully deleted project
//     */
//    override suspend fun remove(id: Long): ProjectProject {
//        val original = repository.findById(id).orElseThrow404(id)
//        TODO("not checked yet - update the values you really want updated")
//        return repository.delete(original)
//    }
//
//    /**
//     * Update the Project: Updates an existing Project
//     * HTTP Code 200: The updated model
//     * HTTP Code 404: The requested object could not be found by the submitted id.
//     * HTTP Code 422: On or many fields contains a invalid value.
//     */
//    override suspend fun update(body: ProjectProject, id: Long): ProjectProject {
//        val original = repository.findById(id).orElseThrow404(id)
//        TODO("not checked yet - update the values you really want updated")
//        return repository.save(original)
//    }
    override suspend fun create(body: ProjectProjectDTO): ProjectProjectDTO {
        TODO("Not yet implemented")
    }

    override suspend fun getAll(maxResults: Int, page: Int): Page<ProjectProjectDTO> {
        TODO("Not yet implemented")
    }

    override suspend fun remove(id: Long): ProjectProjectDTO {
        TODO("Not yet implemented")
    }

    override suspend fun update(body: ProjectProjectDTO, id: Long): ProjectProjectDTO {
        TODO("Not yet implemented")
    }
}
