package ffufm.kurt.api.handlerimpl.superior

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffufm.kurt.api.repositories.superior.SuperiorSuperiorRepository
import ffufm.kurt.api.spec.dbo.superior.SuperiorSuperior
import ffufm.kurt.api.spec.dbo.superior.SuperiorSuperiorDTO
import ffufm.kurt.api.spec.handler.superior.SuperiorSuperiorDatabaseHandler
import kotlin.Long
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component

@Component("superior.SuperiorSuperiorHandler")
class SuperiorSuperiorHandlerImpl : PassDatabaseHandler<SuperiorSuperior,
        SuperiorSuperiorRepository>(), SuperiorSuperiorDatabaseHandler {
    //    /**
//     * Create Superior: Creates a new Superior object
//     * HTTP Code 201: The created Superior
//     */
//    override suspend fun create(body: SuperiorSuperior): SuperiorSuperior {
//        TODO("not checked yet")
//        return repository.save(body)
//    }
//
//    /**
//     * Finds Superiors by ID: Returns Superiors based on ID
//     * HTTP Code 200: The Superior object
//     * HTTP Code 404: A object with the submitted ID does not exist!
//     */
//    override suspend fun getById(id: Long): SuperiorSuperior? {
//        TODO("not checked yet")
//        return repository.findById(id).orElseThrow404(id)
//    }
//
//    /**
//     * Delete Superior by id.: Deletes one specific Superior.
//     * HTTP Code 200: Successfully deleted superior
//     */
//    override suspend fun remove(id: Long): SuperiorSuperior {
//        val original = repository.findById(id).orElseThrow404(id)
//        TODO("not checked yet - update the values you really want updated")
//        return repository.delete(original)
//    }
//
//    /**
//     * Update the Superior: Updates an existing Superior
//     * HTTP Code 200: The updated model
//     * HTTP Code 404: The requested object could not be found by the submitted id.
//     * HTTP Code 422: On or many fields contains a invalid value.
//     */
//    override suspend fun update(body: SuperiorSuperior, id: Long): SuperiorSuperior {
//        val original = repository.findById(id).orElseThrow404(id)
//        TODO("not checked yet - update the values you really want updated")
//        return repository.save(original)
//    }
    override suspend fun create(body: SuperiorSuperiorDTO): SuperiorSuperiorDTO {
        TODO("Not yet implemented")
    }

    override suspend fun getById(id: Long): SuperiorSuperiorDTO? {
        TODO("Not yet implemented")
    }

    override suspend fun remove(id: Long): SuperiorSuperiorDTO {
        TODO("Not yet implemented")
    }

    override suspend fun update(body: SuperiorSuperiorDTO, id: Long): SuperiorSuperiorDTO {
        TODO("Not yet implemented")
    }
}
